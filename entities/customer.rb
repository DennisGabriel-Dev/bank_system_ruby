class Customer
  attr_reader :name, :address, :number_account
  def initialize(name, address, number_account)
    @number_account = number_account
    @name = name
    @address = address
    @balance = 0.0
  end

  def deposit(amount)
    @balance += amount
  end

  def print_customer
    puts("Número da conta #{@number_account}")
    puts("Name #{@name}")
    puts("Address #{@address}")
    puts("Balance #{@balance}")
  end
end
