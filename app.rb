require_relative 'entities/customer'
@list_customer = []
@number_account = 1000

c1 = Customer.new("Dennis", "Rua z", @number_account)
c2 = Customer.new("Gabriel", "Rua y", @number_account + 1)
c3 = Customer.new("João", "Rua aberta", @number_account + 2)
@list_customer.append(c1, c2, c3)
@number_account = @list_customer.last.number_account

def menu
  system('clear')
  print("Digit the number by operation:
  1 - Cadastrar Cliente
  2 - Mostrar todos
  3 - Depositar
  -> ")
  option = gets.chomp.to_i

  if option == 1
    cadastrar_clientes
  elsif option == 2
    print_all
  elsif option == 3
    print "Digite a quantia que deseja depositar: "
    amount = gets.chomp
    print "Digite o número da conta: "
    number = gets.chomp.to_i
    deposit(amount)
  end
end

def cadastrar_clientes
  print("Type your name: ")
  name = gets.chomp()
  print("Your address: ")
  address = gets.chomp()
  @number_account += 1
  @list_customer.push(Customer.new(name, address, @number_account))
end

def print_all
  @list_customer.each do |customer|
    print customer.print_customer
    p "========================="
  end
  gets.chomp
end

def deposit(amount, number_account)
  @list_customer.each do |customer|
    if customer.number_account == number_account
      customer.deposit(amount)
    end
  end
end


while true
  menu
end
